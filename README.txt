Multimedia Customer Feedback
============================

http://drupal.org/project/multimedia_feedback

What Is This?
-------------

This module facilitates business gathering customer feedback in multimedia 
formats: text, photo, audio, and video. The tool makes it easy for business to 
appreciate customer's time and effort instantly, safely, and simply. It is a 
both PC and mobile friendly out-of-the-box solution, offering feedback campaign 
setup, customer experience capture, digital content protection, online money 
payment, and social media sharing. With only a few clicks, business has the 
powerful comment box ready. It dramatically increases customer engagement. 
Customers never ignore request to leave a comment or suggestion.

As a business owner, you get candid and objective feedback from your customers. 
You have positive audio visual endorsements to show off your business online and 
offline. You use critiques to improve product design and service quality. You 
have objective evidence to evaluate operation performance of individual or 
organization. You have the opportunity to follow up nicely with customers, 
resulting in long-term customer relationship.

Collecting feedback is one of total customer satisfaction tools to help retain 
your existing customers, find customer pain points to make improvements. The 
positive feedback is the most powerful tool to generate sales leads. Every 
business should treat collecting feedback as their first priority. 

Special Features 
-----------------

* Video customer feedback/testimonial/experience/satisfaction/survey/suggestion
* Audio customer feedback/testimonial/experience/satisfaction/survey
* Photo customer feedback/testimonial/experience/satisfaction/survey
* Text customer feedback/testimonial/experience/satisfaction/survey
* Mobile device friendly, tested on iOS and Andriod 
* No mobile app installation required for multimedia capture
* HTML5 doctype
* Visual presentation never leave your webpage
* Shortcode on WordPress-based sites or short URL on anywhere	
* Watermark protection of digital contents
* Instant notification and real-time transaction processing	
* Single button nicely working with contact us or customer survey form or 
  customer email communication
* Easy customer engagement and customer retention with reward through social 
  media and from anywhere 
* PayPal payments, fast and secure	

How To Use The Module
-------------------------------
1. Set up a feedback campaign for your business
After signing up at http://www.grabimo.com, you can set up your cash reward for 
customer feedback in video, audio, photo, and text formats.

2. Make your existing customers aware of the compaign
You may publish the campaign on your Drupal website after you install this 
module. You also can publish the compaign on Google+, Facebook, or anywhere 
you want. Grabimo even can email your existing customers about the campaign. 

3. Reward customers for feedback
You will be notified instantly once customers submit their invaluable unique 
experience. Please appreciate customers for spending time and effort providing 
you with video, audio, photo, and text feedback. You pay with credit cards or 
PayPal.

4. Manage customer feedback 
You have the tool to grow your business by leveraging audiovisual customer 
feedback. You can show off your business with positive feedback, while improve 
your service or product with critique. You position your business better and 
influence prospects.

* Check out the demo site: http://demo.grabimo.com/drupal7
* Watch the YouTube video: http://www.youtube.com/watch?v=AH4k64nyR3Y

How To Install The Module
--------------------------

1. Install the Multimedia Feedback moudle (unpacking it to your Drupal 
/sites/all/modules directory if you're installing by hand, for example).

2. Enable the Mutlimedia Feedback module in 
Admin menu > Modules > Multimedia feedback

3. Configure the module (button size, your business alias created at 
http://www.grabimo.com, and short note) in 
Admin menu > Configuration > Multimedia feedback 

4. You can put the Mutlimedia Feedback book in 
Admin menu > Structure > Blocks > Multimedia feedback

If you find a problem, incorrect comment, obsolete or improper code or such,
please search for an issue about it at 
http://drupal.org/project/issues/meltimedia_feedback
If there isn't already an issue for it, please create a new one.
